# GitOps Cluster Configuration

This is heavily baesd on Christian Hernandez's book [The Path to GitOps](https://developers.redhat.com/e-books/path-gitops) and [the accompanying repo](https://github.com/christianh814/example-kubernetes-go-repo).


## Create the cluster

Make your life easier and create an alias to use the sandbox.
```shell
alias vault="aws-vault exec sso-sandbox-account-admin -- "
```

Create the cluster with [eksctl](https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html).
```shell
vault eksctl create cluster -f cluster.yaml
```

## Configure the cluster

Bootstrap the cluster and check that all apps are synced
```shell
until vault kubectl apply --kustomize bootstrap/overlays/default; do sleep 3; done
vault kubectl get applications -n argocd
vault kubectl get appsets -n argocd
```

## Connect to the Argo CD UI

Retrieve the password and login with username `admin`.
```shell
vault kubectl get secret/argocd-initial-admin-secret -n argocd -o jsonpath='{.data.password}' | base64 -d ; echo
vault kubectl -n argocd port-forward service/argocd-server 8080:443
open https://localhost:8080/
```

## Sealed Secrets

[Backup and restore](https://github.com/bitnami-labs/sealed-secrets/?tab=readme-ov-file#how-can-i-do-a-backup-of-my-sealedsecrets) the existing sealing key.
```shell
# Backup the key
$ kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml > main.key
# Restore the key
$ vault kubectl apply -f main.key
$ vault kubectl delete pod -n kube-system -l name=sealed-secrets-controller
```

## Cleanup the cluster

```shell
vault eksctl delete cluster -f cluster.yaml
```